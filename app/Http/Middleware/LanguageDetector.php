<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;

class LanguageDetector
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $allowed_lang = explode(',', env('LANG_ALLOWED'));
        if(session()->has('locale') && in_array(session('locale'), $allowed_lang)){
            app()->setLocale(session('locale'));
        }else{
            $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
            if(in_array($lang, $allowed_lang)){
                app()->setLocale($lang);
            }else{
                app()->setLocale(config('app.locale'));
            }
        }

        return $next($request);
    }
}
