<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Mail\resetPassword;
use App\Mail\signUp;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{

    public function logout(){
        Auth::logout();
        return redirect('/');
    }

    public function register(){
        if(Auth::check()){
            Auth::logout();
            return redirect('register');
        }
        return view('auth.register');
    }

    public function postRegister(Request $request){
        $validatedData = $request->validate([
            'email' => 'required|email|unique:users',
            'password' => 'required|min:4',
            'password_2' => 'required|same:password',
            'accept_terms' => 'required',
            'g-recaptcha-response' => 'required'
        ]);

        $data = ['email' => $request->input('email'), 'password' => $request->input('password')];

        Mail::to($request->input('email'))->send(new signUp($data));

        $data = $request->except(['_token', 'password_2', 'g-recaptcha-response']);
        $data['password'] = Hash::make($data['password']);
        $user = User::create($data);
        Auth::login($user, true);
        return redirect('/dashboard');
    }

    public function postLogin(Request $request){
        $validatedData = $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:4'
        ]);

        $user = User::where('email', $request->input('email'))->first();
        if(!empty($user)){
            if(Hash::check($request->input('password'), $user->password)){
                if($request->has('remember_me')) $remember = true; else $remember = false;
                Auth::login($user, $remember);
                return redirect('/dashboard');
            }else{
                return redirect()->back()->withInput()->with('login_failed', true);
            }
        }else{
            return redirect()->back()->withInput()->with('login_failed', true);
        }
    }

    public function forgotPassword(){
        return view('auth.forgot_password');
    }

    public function postForgotPassword(Request $request){
        $validatedData = $request->validate([
            'email' => 'required|email',
            'g-recaptcha-response' => 'required'
        ]);

        $user = User::where('email', $request->input('email'))->first();

        if(!empty($user)){
            $data['email'] = $request->input('email');
            $data['token'] = str_random('3').time().str_random(rand(6, 12));
            $data['ip'] = $request->ip();
            $data['created_at'] = now();
            DB::table('password_resets')->where('email', $data['email'])->delete();
            DB::table('password_resets')->insert($data);
            Mail::to($data['email'])->send(new resetPassword($data));
            return redirect('/forgotpassword')->with('email_sended_success', true);
        }else{
            return redirect('/forgotpassword')->with('no_email_error', true);
        }
    }

    public function resetPassword($email, $token){
        $reset = DB::table('password_resets')->where('email', $email)->where('token', $token)->first();

        if(empty($reset)){
            abort(404);
        }

        return view('auth.reset_password');
    }

    public function postResetpassword(Request $request, $email, $token)
    {

        $reset = DB::table('password_resets')->where('email', $email)->where('token', $token)->first();

        if (empty($reset)) {
            abort(404);
        }

        $validatedData = $request->validate([
            'r_password' => 'required|min:4',
            'password_2' => 'required|same:r_password'
        ]);

        $user = User::where('email', $email)->first();

        if(empty($user)){
            return redirect('/')->with('error_operation', true);
        }else{
            $user->password = Hash::make($request->input('r_password'));
            $user->save();
            DB::table('password_resets')->where('email', $email)->delete();
            return redirect('/')->with('success_operation', true);
        }
    }
}
