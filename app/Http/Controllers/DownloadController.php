<?php

namespace App\Http\Controllers;

use App\Download;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class DownloadController extends Controller
{
    public function download(Request $request, $slug)
    {
        $asset = File::where('slug', $slug)->firstOrFail();
        $download = new Download();
        if(Auth::check()){
            $download->user_id = Auth::user()->id;
        }
        $download->file_id = $asset->id;
        $download->ip = $request->ip();
        $download->save();
        return Storage::disk('s3')->download('files/'.$asset->slug, $asset->filename);
    }

    public function stats(Request $request, $period='w')
    {
        if(Auth::check()){
            $downloads = Auth::user()->downloadsOnMyFiles();
            $labels = array();
            $datas = array();
            if($period === "d"){
                $downloads->where('created_at', '>=', Carbon::now()->subWeek());
                $downloads = $downloads->groupBy(function($download) {
                        return Carbon::parse($download->created_at)->format('d/m/Y');
                    });
                $start = Carbon::parse('now -7 day');
                $end = Carbon::parse('now');
                while($start < $end)
                {
                    $label = $start->format('d/m/Y');
                    $labels[] = $label;
                    if($downloads->has($label)){
                        $datas[] = $downloads->get($label)->count();
                    }else{
                        $datas[] = 0;
                    }
                    $start->addDay();
                }
            }else{
                $downloads->where('created_at', '>=', Carbon::now()->subMonth(6));
                $downloads = $downloads->groupBy(function($download) {
                        return Carbon::parse($download->created_at)->format('m/Y');
                    });
                $start = Carbon::parse('now -6 month');
                $end = Carbon::parse('now');
                while($start < $end)
                {
                    $label = $start->format('m/Y');
                    $labels[] = $label;
                    if($downloads->has($label)){
                        $datas[] = $downloads->get($label)->count();
                    }else{
                        $datas[] = 0;
                    }
                    $start->addMonth();
                }
            }

            unset($labels[0]);
            unset($datas[0]);

            $data = array();
            $data['labels'] = array_values($labels);
            $data['datasets'][0]['data'] = array_values($datas);

            return json_encode($data);
        }else{
            return abort(404);
        }

    }
}
