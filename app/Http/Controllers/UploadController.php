<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class UploadController extends Controller
{
    public function ajax(Request $request){
        if($request->ajax() && $request->hasFile('files')){
            $files['files'] = [];
            foreach($request->files as $file){
                $slug = uniqid().'.'.$file[0]->getClientOriginalExtension();

                if(!empty(File::where('slug', $slug))){
                    $storagePath = Storage::disk('s3')->put("files/".$slug, file_get_contents($file[0]));

                    $storedFile = new File();
                    $storedFile->name = str_slug($file[0]->getClientOriginalName(), '_');
                    $storedFile->filename = iconv('UTF-8', 'ISO-8859-1//TRANSLIT//IGNORE', $file[0]->getClientOriginalName());
                    if(Auth::check()) $storedFile->user_id = Auth::user()->id;
                    $storedFile->type = $file[0]->getMimeType();
                    $storedFile->location = 's3';
                    $storedFile->size = $file[0]->getClientSize();
                    $storedFile->slug = $slug;
                    $storedFile->token = str_random(rand(6,12));
                    $storedFile->save();

                    $files['files'][] = [
                        'name' => $storedFile->filename,
                        'slug' => $slug,
                        'd_token' => $storedFile->token
                    ];
                }
            }
            echo json_encode($files);
        }else{
            abort(404);
        }
    }

    public function delete(Request $request, $slug, $token){
        if($request->ajax() && !empty(File::where('slug', $slug)->where('token', $token)->first())){
            $file = File::where('slug', $slug)->firstOrFail();
            Storage::disk('s3')->delete("files/".$slug);
            $file->delete();

            echo json_encode('success');
        }else{
            abort(404);
        }
    }
}
