<?php

namespace App\Http\Controllers\Dashboard;

use App\Payment;
use App\Setting;
use App\Snap;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class FinanceController extends Controller
{
    public function index()
    {
        $setting = Setting::orderBy('id', 'desc')->firstOrFail();
        return view('dashboard.finance.index', compact('setting'));
    }

    public function recover()
    {
        $setting = Setting::orderBy('id', 'desc')->firstOrFail();
        return view('dashboard.finance.recover', compact('setting'));
    }

    public function stats(Request $request, $period='w')
    {
        if(Auth::check()){
            $benefits = Auth::user()->benefits;
            $labels = array();
            $datas = array();

            $benefits->where('created_at', '>=', Carbon::now()->subMonth(6));
            $benefits = $benefits->groupBy(function($benefit) {
                return Carbon::parse($benefit->created_at)->format('m/Y');
            });
            $start = Carbon::parse('now -6 month');
            $end = Carbon::parse('now');

            while($start < $end)
            {
                $label = $start->format('m/Y');
                $labels[] = $label;
                if($benefits->has($label)){
                    $amount = 0;
                    foreach($benefits->get($label) as $benefit){
                        $amount += $benefit->amount;
                    }
                    $datas[] = $amount;
                }else{
                    $datas[] = 0;
                }
                $start->addMonth();
            }

            unset($labels[0]);
            unset($datas[0]);

            $data = array();
            $data['labels'] = array_values($labels);
            $data['datasets'][0]['data'] = array_values($datas);

            return json_encode($data);
        }else{
            return abort(404);
        }

    }

    public function paypal(Request $request){
        $request->validate([
            'amount' => 'required|numeric|min:0.01|max:'.Auth::user()->availableMoney(),
            'email' => 'required|email'
        ]);

        $data = [
            'email' => $request->input('email')
        ];

        $user = Auth::user();

        $snap = new Snap();
        $snap->user_id = $user->id;
        $snap->ip = $request->ip();
        $snap->history = json_encode($user->payments);
        $snap->stats = json_encode(Setting::orderBy('id', 'desc')->first());
        $snap->os = json_encode($request->server);
        $snap->save();

        $payment = new Payment();
        $payment->user_id = $user->id;
        $payment->amount = $request->input('amount');
        $payment->data = json_encode($data);
        $payment->snap_id = $snap->id;
        $payment->target = Payment::TARGET_PAYPAL;
        $payment->save();

        return redirect()->back()->with('payment_success', $request->input('amount'));
    }
}
