<?php

namespace App\Http\Controllers\Dashboard;

use App\File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller
{
    public function index(){
        $files = Auth::user()->files;
        return view('dashboard.files', compact('files'));
    }

    public function delete($slug, $token){
        if(File::where('slug', $slug)->where('token', $token)->exists()){
            $file = File::where('slug', $slug)->firstOrFail();
            Storage::disk('s3')->delete("files/".$slug);
            $file->delete();

            return redirect()->back()->with('success_file_deleted', true);
        }
    }
}
