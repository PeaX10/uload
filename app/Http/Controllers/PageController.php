<?php

namespace App\Http\Controllers;

use App\Rate;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use App\Mail\contactPage;
use App\File;

class PageController extends Controller
{
    public function index()
    {
        return view('pages.index');
    }

    public function lang($lang)
    {
        $allowed_lang = explode(',', env('LANG_ALLOWED'));
        if (in_array($lang, $allowed_lang)) {
            session()->put('locale', $lang);
        }

        return redirect()->back();
    }

    public function about()
    {
        return view('pages.about');
    }

    public function faq()
    {
        return view('pages.faq');
    }

    public function contact()
    {
        return view('pages.contact');
    }

    public function postContact(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:100',
            'email' => 'required|email',
            'message' => 'required|min:50',
            'g-recaptcha-response' => 'required'
        ]);

        $data = ['name' => Input::get('name'), 'email' => Input::get('email'), 'message' => Input::get('message'), 'ip' => \Request::ip()];
        Mail::to('uload@protonmail.com')->send(new contactPage($data));
        return redirect('/contact')->with('contact_mail_sended', true);
    }

    public function terms()
    {
        return view('pages.terms');
    }

    public function privacy()
    {
        return view('pages.privacy');
    }

    public function copyright()
    {
        return view('pages.copyright');
    }

    public function report()
    {
        return view('pages.report');
    }

    public function file($slug)
    {
        $file = File::where('slug', $slug)->firstOrFail();
        if (!empty($file)) {
            return view('pages.file', compact('file'));
        } else {
            abort(404);
        }
    }

    public function video($slug)
    {
        $file = File::where('slug', $slug)->firstOrFail();
        if (!empty($file)) {
            return view('pages.video', compact('file'));
        } else {
            abort(404);
        }
    }

    public function init(){ // TODO : DELETE
        if(Setting::all()->count() == 0){
            dump('Les paramètres viennent d\'être appliqué');
            $downloadRate = new Rate();
            $downloadRate->numerator = 2;
            $downloadRate->denominator = 999;
            $downloadRate->type = "money";
            $downloadRate->save();
            $viewRate = new Rate();
            $viewRate->numerator = 6;
            $viewRate->denominator = 10000;
            $viewRate->type = "money";
            $viewRate->save();
            $setting = new Setting();
            $setting->download_rate_id = $downloadRate->id;
            $setting->view_rate_id = $viewRate->id;
            $setting->save();
        }
    }
}
