<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Payment extends Model
{
    const TARGET_PAYPAL = 'PP';

    const LIST_MEANS_PAYMENT = [
        self::TARGET_PAYPAL => 'Paypal'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function snap(){
        return $this->belongsTo('App\Snap');
    }
}
