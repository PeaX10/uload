<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    public function downloadRate(){
        return $this->belongsTo('App\Rate');
    }

    public function viewRate(){
        return $this->belongsTo('App\Rate');
    }
}
