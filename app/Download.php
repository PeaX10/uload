<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Download extends Model
{
    public function user(){
        return $this->belongsTo('App\User');
    }

    public function file(){
        return $this->belongsTo('App\File');
    }

    public static function mostDownloaded($downloads=null)
    {
        if($downloads === null) $downloads = Download::all();
        $files = [];
        foreach($downloads as $download){
            if($download->file) $files[] = $download->file->id;
        }
        $count = array_count_values($files);
        if(!$count) return null;
        $mostDowloaded = File::find(array_search(max($count), $count));
        return $mostDowloaded;
    }
}
