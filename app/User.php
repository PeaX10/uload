<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function files()
    {
        return $this->hasMany('App\File');
    }

    public function downloads()
    {
        return $this->hasMany('App\Download');
    }

    public function payments(){
        return $this->hasMany('App\Payment');
    }

    public function benefits(){
        return $this->hasMany('App\Benefit');
    }

    public function spaceUsed()
    {
        $totalSize = 0;
        foreach($this->files as $file){
            $totalSize += $file->size;
        }
        return $totalSize;
    }

    public function downloadsOnMyFiles()
    {
        $downloads = collect();
        foreach($this->files as $file){
            $downloads = $downloads->merge($file->downloads);
        }
        return $downloads;
    }

    public function availableMoney() : float{
        $amount = 0;
        foreach($this->benefits()->get() as $benefit){
            $amount += $benefit->amount;
        }
        return $amount - $this->alreadyEarned();
    }

    public function alreadyEarned() : float
    {
        $amount = 0;
        foreach($this->payments as $payment){
            $amount += $payment->amount;
        }

        return $amount;
    }
}
