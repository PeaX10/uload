<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Snap extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function setting(){
        return $this->belongsTo('App\Setting');
    }
}
