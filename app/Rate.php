<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    public function __toString()
    {
        return $this->numerator. '/'.$this->denominator;
    }
}
