@extends('pages.includes.default')

@section('title', trans('global.meta.title'))

@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<link rel="stylesheet" href="{{ url('css/jquery.fileupload.css') }}">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css">
@endsection

@section('content')
    <div class="container">
        @if(Session::has('error_operation'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                @lang('global.home.alert_danger')
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @elseif(Session::has('success_operation'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                @lang('global.home.alert_success')
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <h1>{!! trans('global.home.h1') !!}</h1>
        <div class="upload fileinput-button">
            <img src="{{ url('img/upload.png') }}" />
            <div class="uploadDescription">
                <p>{!! trans('global.home.drag_drop') !!}</p>
                <p>{!! trans('global.home.file_size') !!}</p>
            </div>
            <input id="fileupload" type="file" name="files[]" multiple>
            <div id="progress" class="text-right">
                <a class="current"></a>
                <a class="total"></a>
                <div class="progress">
                    <div class="progress-bar progress-bar-success">
                        <a style="text-align: right; margin-right: 4px;"></a>
                    </div>
                </div>
                <a class="timeleft">@lang('global.home.ready_in') <span class="time"></span></a>
            </div>
        </div>
        <div class="table-responsive">
            <div class="your_files text-left"><h6>@lang('global.home.your_files')</h6></div>
            <table class="table table-hover table-striped" id="files">
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteModalLabel">@lang('global.home.delete_modal_title')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @lang('global.home.delete_modal_text')
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('global.home.delete_modal_close')</button>
                    <button type="button" id="delete_F" class="btn btn-danger">@lang('global.home.delete_modal_btn')</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ url('js/jquery.ui.widget.js') }}"></script>
    <script src="{{ url('js/jquery.iframe-transport.js') }}"></script>
    <script src="{{ url('js/jquery.fileupload.js') }}"></script>
    <script>

        function openDeleteModal(slug, token){
            $('#deleteModal').modal('show');

            $('#deleteModal #delete_F').attr('onclick', 'delete_F("'+slug+'", "'+token+'")')
        }

        function humanFileSize(bytes, si) {
            var thresh = si ? 1000 : 1024;
            if(Math.abs(bytes) < thresh) {
                return bytes + ' B';
            }
            var units = si
                ? {!! trans('global.home.file_sizes') !!};
            var u = -1;
            do {
                bytes /= thresh;
                ++u;
            } while(Math.abs(bytes) >= thresh && u < units.length - 1);
            return bytes.toFixed(1)+' '+units[u];
        }

        function humanTime(s) {
            var fm = [
                Math.floor(s / 60 / 60 / 24), // DAYS
                Math.floor(s / 60 / 60) % 24, // HOURS
                Math.floor(s / 60) % 60, // MINUTES
                Math.floor(s % 60) // SECONDS
            ];

            var time = '';

            for (var k = 0; k < 4; k++) {

                v = fm[k];

                if(k == 0 && v > 0){
                    time = time + v + 'd ';
                }else if(k == 1 && v > 0){
                    time = time + v + 'h ';
                }else if(k == 2 && v > 0){
                    time = time + v + 'm ';
                }else if(k == 3 && v > 0){
                    time = time + v + 's ';
                }
            }

            if(time == '') time = '0s';

            return time;
        }

        function delete_F(slug, token){

            $('#deleteModal').modal('hide');

            $.ajax({
                url: "{{ url('d') }}/" + slug + "/" + token,
                method: "GET",
            })
            .done(function( html ) {
                $('tr[data-slug="' + slug + '"]').remove();

                if($('#files tr').length == 0){
                    $('.your_files').hide();
                }
            })
            .fail(function( html ) {
                alert('{{ trans('global.home.error_file_deleted') }}');
            });
        }

        $(function () {
            'use strict';

            $('#progress').hide();
            $('.your_files').hide();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var total = 0;

            $('#fileupload').fileupload({
                add: function(e, data) {
                    var error = false;
                    $.each( data.originalFiles, function( i, val ) {
                        if(val['size'].length && val['size'] > 5000000) {
                            delete data.originalFiles[i];
                            alert('@lang('global.home.file_too_big')');
                            error = true;
                        }else{
                            total += val['size'];
                        }
                    });
                    if(!error){
                        $('#progress .total').text(humanFileSize(total, true));
                        $('#progress').show();
                        $('#progress .current').show();
                        data.submit();
                    }
                },
                url: '{{ url('/upload') }}',
                dataType: 'json',

                done: function (e, data) {

                    $('.your_files').show();

                    $.each(data.result.files, function (index, file) {
                        $('#files tbody').append(
                            '<tr data-slug="' + file.slug + '">' +
                            '   <td><a class="text-success fa fa-check-circle"></a> <a class="name" target="_blank" href="{{ url('f') }}/' + file.slug + '">' + file.name + '</a></td>' +
                            '   <td>' +
                            '       <a class="text-primary fas fa-arrow-alt-circle-down" target="_blank" href="{{ url('f') }}/' + file.slug + '"></a>' +
                            '       <a class="text-danger far fa-trash-alt" onclick="openDeleteModal(\''+ file.slug + '\',\'' + file.d_token + '\')"></a>' +
                            '   </td>' +
                            '</tr>'
                        );
                    });
                    if( $('#progress .current').position().left >= $('#progress').width() - $('#progress .total').width() - $('#progress .current').width()){
                        $('#progress .current').css('left', 0);
                        $('#progress .current').hide();
                        $('#progress .current').text('');
                    }
                    $('#progress').hide();
                },
                fail: function (e, data) {
                    $.each(data.messages, function (index, error) {
                        console.log(index, error);
                    });
                    alert('Il semblerait qu\'une erreur c\'est produite lors de du chargement du fichier');
                },
                progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progress .progress-bar').css(
                        'width',
                        progress + '%'
                    );

                    $('#progress .progress-bar a').text(progress + '%');
                    $('#progress .current').text(humanFileSize(progress*0.01*total, true));

                    var middleWPercent = $('#progress .current').width() / $('#progress').width();

                    console.log($('#progress .current').position().left);

                    if( $('#progress .current').position().left >= $('#progress').width() - $('#progress .total').width() - $('#progress .current').width()){
                        $('#progress .current').css('left', 0);
                        $('#progress .current').hide();
                        $('#progress .current').text('');
                    }else{
                        progress = progress - middleWPercent*50;
                        $('#progress .current').css('left', progress + '%');
                    }

                    var secondsRemaining = (data.total - data.loaded) * 8 / data.bitrate;
                    $('#progress span.time').text(humanTime(secondsRemaining));

                }
            }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');
        });
    </script>
@endsection