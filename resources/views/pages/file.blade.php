@extends('pages.includes.default')

@section('title', $file->name)

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ url('css/plugins/jssocials.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ url('css/plugins/jssocials-theme-flat.css') }}" />
    <link href="https://vjs.zencdn.net/7.4.1/video-js.css" rel="stylesheet">
    <script src="https://vjs.zencdn.net/ie8/ie8-version/videojs-ie8.min.js"></script>
    <link href="{{ url('js/videojs/videojs-resolution-switcher.css') }}" rel="stylesheet">
    <script src="https://unpkg.com/silvermine-videojs-quality-selector/dist/js/silvermine-videojs-quality-selector.min.js"></script>

@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-12 col-xs-12 text-left">
                @if($file->isVideo())
                    <video id="my-video" class="video-js" controls preload="auto" width="100%" height="auto"
                           poster="" data-setup="{}">
                        <source src="{{ Storage::disk('s3')->temporaryUrl('files/'.$file->slug, now()->addMinutes(5)) }}" type='{{ $file->type }}' label='ORIGINAL'>
                        <source src="http://mirrorblender.top-ix.org/movies/sintel-1024-surround.mp4" type='video/mp4' label='SD' />
                        <source src="http://media.xiph.org/mango/tears_of_steel_1080p.webm" type='video/webm' label='HD'/>
                        <p class="vjs-no-js">
                            To view this video please enable JavaScript, and consider upgrading to a web browser that
                            <a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                        </p>
                    </video>
                @elseif($file->isImage())
                    <img src="{{ Storage::disk('s3')->temporaryUrl('files/'.$file->slug, now()->addMinutes(5)) }}" title="{{ $file->name }}" alt="{{ $file->name }}" style="
                    max-width: 100%;
                    max-height: 500px">
                @endif
                <div class="col-md-12"><a class="text-primary">{{ $file->filename }}</a></div>
                <div class="col-md-12">
                    <span class="size">File size : {{ $file->formatBytes($file->size) }}</span>
                </div>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12">
                <h3>SHARE IT</h3>
                <div class="share"></div>
                <div class="download-btn">
                    <a href="{{ route('download', ['slug' => $file->slug]) }}" class="btn">
                        <h4>FREE DOWNLOAD</h4>
                        Click to start download
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ url('js/jssocials.min.js') }}"></script>
    <script type="text/javascript">
        $(".share").jsSocials({
            showCount: true,
            showLabel: false,
            shares: ["email", "twitter", "facebook", "googleplus", "linkedin", "pinterest", "stumbleupon", "whatsapp"],
            shareIn: "popup"
        });
    </script>
    <script type="text/javascript">
        $( document ).ready(function() {
            var options, player;

            options = {
                controlBar: {
                    children: [
                        'playToggle',
                        'progressControl',
                        'volumePanel',
                        'qualitySelector',
                        'fullscreenToggle',
                    ],
                },
            };

            player = videojs('video_1', options);
        });

    </script>
@endsection
