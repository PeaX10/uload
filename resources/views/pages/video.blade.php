@extends('pages.includes.default')

@section('title', $file->name)

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ url('css/plugins/jssocials.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ url('css/plugins/jssocials-theme-flat.css') }}" />
    <link href="https://vjs.zencdn.net/7.3.0/video-js.css" rel="stylesheet">
    <script src="https://vjs.zencdn.net/ie8/ie8-version/videojs-ie8.min.js"></script>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-12 col-xs-12 text-left">
                @if($file->isVideo())
                <video id="my-video" class="video-js" controls preload="auto" width="640" height="264"
                       poster="MY_VIDEO_POSTER.jpg" data-setup="{}">
                    <source src="{{ Storage::cloud()->url($file->slug) }}" type='video/mp4'>
                    <source src="MY_VIDEO.webm" type='video/webm'>
                    <p class="vjs-no-js">
                        To view this video please enable JavaScript, and consider upgrading to a web browser that
                        <a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                    </p>
                </video>
                @endif
                <div class="col-md-12"><a class="text-primary">{{ $file->name }}</a></div>
                <div class="col-md-12">
                    <span class="size">File size : {{ $file->formatBytes($file->size) }}</span>
                </div>

            </div>
            <div class="col-md-4 col-sm-12 col-xs-12">
                <h3>SHARE IT</h3>
                <div class="share"></div>
                <div class="download-btn">
                    <a href="{{ route('download', $file->slug) }}" class="btn">
                        <h4>FREE DOWNLOAD</h4>
                        Click to start download
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ url('js/jssocials.min.js') }}"></script>

    <script src="https://vjs.zencdn.net/7.3.0/video.js"></script>
    <script type="text/javascript">
        $(".share").jsSocials({
            showCount: true,
            showLabel: false,
            shares: ["email", "twitter", "facebook", "googleplus", "linkedin", "pinterest", "stumbleupon", "whatsapp"]
        });
    </script>
@endsection
