@extends('pages.includes.default')

@section('title', trans('global.footer.about'))

@section('content')
    <div class="container">
        <div class="page">
            <h1>@lang('global.about.title_1')</h1>
            @lang('global.about.text_1')
            <br>
            <h1>@lang('global.about.title_2')</h1>
            @lang('global.about.text_2')
        </div>
    </div>
@endsection