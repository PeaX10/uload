@extends('pages.includes.default')

@section('title', trans('global.footer.faq'))

@section('content')
    <div class="container">
        <div class="page">
            <h1>@lang('global.faq.title_1')</h1>
            @lang('global.faq.text_1')
            <br>
            <h1>@lang('global.faq.title_2')</h1>
            @lang('global.faq.text_2')
            <br>
            <h1>@lang('global.faq.title_3')</h1>
            @lang('global.faq.text_3')
            <br>
            <h1>@lang('global.faq.title_4')</h1>
            @lang('global.faq.text_4')
            <br>
            <h1>@lang('global.faq.title_5')</h1>
            @lang('global.faq.text_5')
            <br>
            <h1>@lang('global.faq.title_6')</h1>
            @lang('global.faq.text_6')
            <br>
            <h1>@lang('global.faq.title_7')</h1>
            @lang('global.faq.text_7')
        </div>
    </div>
@endsection