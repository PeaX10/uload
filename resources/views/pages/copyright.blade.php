@extends('pages.includes.default')

@section('title', trans('global.footer.copyright'))

@section('content')
    <div class="container">
        <div class="page">
            <h1>COPYRIGHT POLICY</h1>
            <h1>Notification of Infringement</h1>
            <h5>It is our policy to respond to clear notices of alleged copyright infringement that comply with the Digital Millennium Copyright Act. In addition, we will promptly terminate the accounts of those determined by us to be "repeat infringers" without notice. If you are a copyright owner or an agent thereof, and you believe that any content hosted on our website (uLoad.io) infringes your copyrights, then you may submit a notification pursuant to the Digital Millennium Copyright Act ("DMCA") by providing uLoad's Designated Copyright Agent with the following information in writing (please consult your legal counsel or See 17 U.S.C. Section 512(c)(3) to confirm these requirements):</h5>
            <h5>
                <ul>
                    <li>A physical or electronic signature of a person authorized to act on behalf of the owner of an exclusive right that is allegedly infringed.</li>
                    <li>Identification of the copyrighted work claimed to have been infringed, or, if multiple copyrighted works on uLoad are covered by a single notification, a representative list of such works at that website.</li>
                    <li>Identification of the material that is claimed to be infringing or to be the subject of infringing activity and that is to be removed or access to which is to be disabled, and information reasonably sufficient to permit streamango to locate the material. Providing URLs in the body of an email is the best way to help us locate content quickly.</li>
                    <li>Information reasonably sufficient to permit uLoad to contact the complaining party, such as an address, telephone number, and, if available, an electronic mail address at which the complaining party may be contacted.</li>
                </ul>
            </h5>
            <br>
            <h1>Counter-Notification</h1>
            <h5>If you elect to send us a counter notice, to be effective it must be a written communication that includes the following (please consult your legal counsel or See 17 U.S.C. Section 512(g)(3) to confirm these requirements):</h5>
            <h5>
                <ul>
                    <li>A physical or electronic signature of a person authorized to act on behalf of the owner of an exclusive right that is allegedly infringed.</li>
                    <li>Identification of the copyrighted work claimed to have been infringed, or, if multiple copyrighted works on streamango are covered by a single notification, a representative list of such works at that website.</li>
                </ul>
            </h5>
            <br>
            <h1>Designated Copyright Agent</h1>
            <h5>uLoad's Designated Copyright Agent to receive notifications and counter-notifications of claimed infringement can be reached as follows:</h5>
            <h5>You may submit a copyright notice here uLoad.io/report or clarity, only DMCA notices should go to the uLoad Designated Copyright Agent.</h5>
            <h5>Any other feedback, comments, requests for technical support or other communications should be directed to streamango customer service through the uLoad Contact Center at uLoad.io/contact. You acknowledge that if you fail to comply with all of the requirements of this section, your DMCA notice may not be valid.</h5>
        </div>
    </div>
@endsection