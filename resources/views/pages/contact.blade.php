@extends('pages.includes.default')

@section('title', trans('global.footer.contact'))

@section('content')
    <div class="container">
        <div class="page">
            <h1>@lang('global.footer.contact')</h1>
            <h5>@lang('global.contact.description')</h5>
            <hr>
            @if(Session::has('contact_mail_sended'))
                <div class="alert alert-success" role="alert">
                    @lang('global.contact.mail_sended')
                </div>
            @endif
            {!! Form::open() !!}
            {!! Form::text('name')->placeholder(trans('global.contact.name'))->attrs(['required' => 'required']) !!}
            {!! Form::text('email')->placeholder(trans('global.contact.email'))->type('email')->attrs(['required' => 'required']) !!}
            {!! Form::textarea('message')->placeholder(trans('global.contact.message'))->attrs(['required' => 'required']) !!}
            {!! NoCaptcha::renderJs(App::getLocale()) !!}
            {!! NoCaptcha::display() !!}
            @if ($errors->has('g-recaptcha-response'))
                <span class="help-block">
                    <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                </span>
            @endif
            <hr>
            {!! Form::submit(trans('global.contact.send'))->lg()->outline() !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection