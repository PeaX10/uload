@extends('pages.includes.default')

@section('title', trans('global.footer.terms'))

@section('content')
    <div class="container">
        <div class="page">
            <h1>@lang('global.terms.title_1')</h1>
            @lang('global.terms.text_1')
            <br>
            <h1>@lang('global.terms.title_2')</h1>
            @lang('global.terms.text_2')
            <br>
            <h1>@lang('global.terms.title_3')</h1>
            @lang('global.terms.text_3')
            <br>
            <h1>@lang('global.terms.title_4')</h1>
            @lang('global.terms.text_4')
            <br>
            <h1>@lang('global.terms.title_5')</h1>
            @lang('global.terms.text_5')
        </div>
    </div>
@endsection