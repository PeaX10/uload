<footer>
    <nav class="navbar navbar-light navbar-expand-md bg-faded justify-content-center">
        <div class="container">
            <div class="navbar-collapse w-100" id="menuFooter">
                <ul class="navbar-nav w-100 justify-content-center d-none d-md-flex">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('about') }}">@lang('global.footer.about')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('faq') }}">@lang('global.footer.faq')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('contact') }}">@lang('global.footer.contact')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('terms') }}">@lang('global.footer.terms')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('privacy') }}">@lang('global.footer.privacy')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('copyright') }}">@lang('global.footer.copyright')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('report') }}">@lang('global.footer.report')</a>
                    </li>
                </ul>
                <ul class="navbar-nav w-100 justify-content-center d-lg-none d-xl-none d-md-none" style="flex-direction: initial;">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('about') }}">@lang('global.footer.about')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('faq') }}">@lang('global.footer.faq')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('contact') }}">@lang('global.footer.contact')</a>
                    </li>
                </ul>
                <ul class="navbar-nav w-100 justify-content-center d-lg-none d-xl-none d-md-none" style="flex-direction: initial;">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('terms') }}">@lang('global.footer.terms')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('privacy') }}">@lang('global.footer.privacy')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('copyright') }}">@lang('global.footer.copyright')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('report') }}">@lang('global.footer.report')</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</footer>