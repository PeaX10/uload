<!doctype html>
<html lang="{{ App::getLocale() }}">
<head>
    <meta charset="utf-8">
    <title>uLoad - @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="{{ @url('img/favicon.png') }}" />
    <link rel="stylesheet" href="{{ @url('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ @url('css/global.css') }}">
    <link rel="stylesheet" href="{{ @url('css/font-awesome.min.css') }}">
    @yield('css')
</head>

<body>
    @include('pages.includes.navbar')

    @if(!Auth::check())
        <section class="login" class="container-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-8 col-md-6 login-box">
                        <div id="close-button" onclick="showHideLoginBox()">
                            <span class="close-button-line1"></span>
                            <span class="close-button-line2"></span>
                        </div>
                        <h6>@lang('global.login.title')</h6>
                        <hr>
                        @if(Session::has('login_failed'))
                            <div class="alert alert-danger" role="alert">
                                @lang('global.register.login_failed')
                            </div>
                        @endif
                        {!! Form::open()->method('post')->route('login') !!}
                            {!! Form::text('email')->placeholder(trans('global.register.email'))->type('email')->attrs(['required' => 'required']) !!}
                            {!! Form::text('password')->placeholder(trans('global.register.password'))->type('password')->attrs(['required' => 'required']) !!}
                            {!!Form::checkbox('remember_me', trans('global.login.remember_me'))!!}
                        <hr>
                        {!! Form::submit(trans('global.login.send'))->dark() !!} <a style="float:right" href="/forgotpassword">@lang('global.login.forgotpass')</a>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </section>
    @endif

    <section id="content">
        @yield('content')
    </section>

    @include('pages.includes.footer')
</body>
<script type="text/javascript" src="{{ @url('js/jquery.js') }}"></script>
<script type="text/javascript" src="{{ @url('js/bootstrap.min.js') }}"></script>
<script type="text/javascript">
    @if ($errors->has('email') or $errors->has('password') or Session::has('login_failed') or Session::has('login.box'))
    showHideLoginBox()
    @endif
    function showHideLoginBox(){
        $('.login').fadeToggle(800);
    }
</script>
@yield('js')
</html>
