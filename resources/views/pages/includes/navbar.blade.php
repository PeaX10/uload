<nav class="navbar navbar-light navbar-expand-md bg-faded justify-content-center">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            <img src="{{ url('img/logo.png') }}" width="135" height="44" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-collapse collapse w-100" id="navbarSupportedContent">
            <ul class="nav navbar-nav ml-auto w-100 justify-content-end">
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('faq') }}">@lang('global.header.help')</a>
                </li>
                @if(!Auth::check())
                <li class="nav-item">
                    <a class="nav-link blue" href="{{ url('register') }}">@lang('global.header.sign_up')</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link blue" onclick="showHideLoginBox()" href="#">@lang('global.header.login')</a>
                </li>
                @else
                <li class="nav-item">
                    <a class="btn btn-primary" href="{{ url('dashboard') }}">@lang('global.header.dashboard')</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('logout') }}">@lang('global.header.logout')</a>
                </li>
                @endif
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ strtoupper(App::getLocale()) }}
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        @foreach(explode(',', env('LANG_ALLOWED')) as $lang)
                        <a class="dropdown-item" @if($lang != App::getLocale()) href="{{ url('lang/'.$lang) }}" @endif>{{ strtoupper($lang) }}</a>
                        @endforeach
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>