@extends('dashboard.includes.default', ['activePage' => 'finance'])

@section('title', trans('global.meta.title'))

@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection

@section('header')
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row">
                    <div class="col-xl-3 col-lg-6">
                        <div class="card card-stats mb-4 mb-xl-0">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">Available Money</h5>
                                        <span class="h2 font-weight-bold mb-0">${{ Auth::user()->availableMoney() }}</span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                                            <i class="fas fa-dollar-sign"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6">
                        <div class="card card-stats mb-4 mb-xl-0">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">Already Earned</h5>
                                        <span class="h2 font-weight-bold mb-0">${{ Auth::user()->alreadyEarned() }}</span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-gradient-info text-white rounded-circle shadow">
                                            <i class="fas fa-university"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6">
                        <div class="card card-stats mb-4 mb-xl-0">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">Download rate</h5>
                                        <span class="h2 font-weight-bold mb-0">${{ $setting->downloadRate }} dls</span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-gradient-indigo text-white rounded-circle shadow">
                                            <i class="fas fa-download"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6">
                        <div class="card card-stats mb-4 mb-xl-0">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">Video rate</h5>
                                        <span class="h2 font-weight-bold mb-0">${{ $setting->viewRate }} views</span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                                            <i class="fas fa-eye"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row mt-5">
        <div class="col">
            <div class="card shadow">
                <div class="card-body">
                    <h1 class="card-title">
                        How mutch do you want ?
                    </h1>
                    <hr>
                    @if(Session::has('payment_success'))
                        <div class="alert alert-success" role="alert">
                            <span class="alert-inner--icon"><i class="fas fa-check"></i></span>
                            <span class="alert-inner--text"><strong>Success!</strong> You have been transfered ${{ Session::get('payment_success') }} !</span>
                        </div>
                    @endif
                    <div class="alert alert-primary" role="alert">
                        <span class="alert-inner--icon"><i class="fas fa-dollar-sign"></i></span>
                        <span class="alert-inner--text"><strong>Attention!</strong> You need to have a paypal account to recover your money !</span>
                    </div>
                    <div class="nav-wrapper">
                        <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0 active" id="tabs-icons-text-1-tab" data-toggle="tab" href="#tabs-icons-text-1" role="tab" aria-controls="tabs-icons-text-1" aria-selected="true"><i class="fab fa-paypal"></i> Paypal</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-2-tab" data-toggle="tab" href="#tabs-icons-text-2" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="fab fa-cc-visa"></i> Visa</a>
                            </li>
                        </ul>
                    </div>
                    <div class="card shadow">
                        <div class="card-body">
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
                                    {!! Form::open()->url(route('dashboard.finance.recover.paypal')) !!}
                                    {!! Form::text('email')->placeholder(trans('global.register.email'). ' paypal')->type('email')->attrs(['required' => 'required']) !!}
                                    {!! Form::text('amount')->placeholder('Amount')->type('number')->attrs(['required' => 'required', 'step' => '0.01', 'min' => 0, 'max' => Auth::user()->availableMoney()]) !!}
                                    <i>Maximum amount : ${{ Auth::user()->availableMoney() }}</i>
                                    <input type="submit" class="btn btn-outline-primary btn-lg float-right" value="Earn">
                                    {!! Form::close() !!}
                                </div>
                                <div class="tab-pane fade" id="tabs-icons-text-2" role="tabpanel" aria-labelledby="tabs-icons-text-2-tab">
                                    <p class="description">Coming soon...</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        function deleteFile(slug, token) {
            $('#deleteLink').attr('href', '{{ url('dashboard/files') }}/'+slug+'/'+token+'/delete')
        }
    </script>
@endsection
