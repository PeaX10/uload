@extends('dashboard.includes.default', ['activePage' => 'finance'])

@section('title', trans('global.meta.title'))

@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection

@section('header')
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row">
                    <div class="col-xl-3 col-lg-6">
                        <div class="card card-stats mb-4 mb-xl-0">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">Available Money</h5>
                                        <span class="h2 font-weight-bold mb-0">${{ Auth::user()->availableMoney() }}</span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                                            <i class="fas fa-dollar-sign"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6">
                        <div class="card card-stats mb-4 mb-xl-0">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">Already Earned</h5>
                                        <span class="h2 font-weight-bold mb-0">${{ Auth::user()->alreadyEarned() }}</span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-gradient-info text-white rounded-circle shadow">
                                            <i class="fas fa-university"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6">
                        <div class="card card-stats mb-4 mb-xl-0">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">Download rate</h5>
                                        <span class="h2 font-weight-bold mb-0">${{ $setting->downloadRate }} dls</span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-gradient-indigo text-white rounded-circle shadow">
                                            <i class="fas fa-download"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6">
                        <div class="card card-stats mb-4 mb-xl-0">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">Video rate</h5>
                                        <span class="h2 font-weight-bold mb-0">${{ $setting->viewRate }} views</span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                                            <i class="fas fa-eye"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="padding-top: 20px;">
                    <div class="col-xl-12 col-lg-12">
                        <a href="{{ route('dashboard.finance.recover') }}" class="btn btn-lg btn-danger float-right"><i class="fa fa-dollar-sign"></i> Recover my money</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row mt-5">
        <div class="col">
            <div class="card shadow">
                <div class="card-body">
                    <h1 class="card-title">
                        How to retrieve my money.
                    </h1>
                    <hr>
                    <p>To make money it's very simple, every time your download or videos reach the required number, your available money balance will update.</p>
                    <p>Then you just need to request a transfer of this money to the means of payment that you prefer.</p>
                    <br>
                    <h1 class="card-title">
                        Transfers list
                    </h1>
                    <hr>
                    <div class="table-responsive">
                        <table class="table align-items-center">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">Amount</th>
                                <th scope="col">Status</th>
                                <th scope="col">Target</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach(Auth::user()->payments as $payment)
                                <tr>
                                    <td>
                                        ${{ $payment->amount }} USD
                                    </td>
                                    <td>
                                        <span class="badge badge-dot mr-4">
                                          <i class="bg-warning"></i> en attente
                                        </span>
                                    </td>
                                    <td>
                                        @if($payment->target == \App\Payment::TARGET_PAYPAL)
                                            <i class="fab fa-paypal"></i> {{ \App\Payment::LIST_MEANS_PAYMENT[\App\Payment::TARGET_PAYPAL] }}@if(!empty(json_decode($payment->data)->email)) - {{ json_decode($payment->data)->email }}@endif
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        function deleteFile(slug, token) {
            $('#deleteLink').attr('href', '{{ url('dashboard/files') }}/'+slug+'/'+token+'/delete')
        }
    </script>
@endsection
