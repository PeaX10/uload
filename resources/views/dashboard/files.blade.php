@extends('dashboard.includes.default', ['activePage' => 'files'])

@section('title', trans('global.meta.title'))

@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection

@section('header')
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
        <div class="container-fluid">
            <div class="header-body">
                <a href="{{ route('home') }}" class="btn btn-danger float-right"><i class="fas fa-plus"></i> Upload file</a>
                <h1 class="text-white">My files</h1>
                @if( Session::has('success_file_deleted'))
                    <div class="alert alert-success" role="alert">
                        <strong>Success!</strong> Your file has been deleted !
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row mt-5">
        <div class="col">
            <div class="card shadow">
                @if($files->count())
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Size</th>
                                <th scope="col">Video</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($files as $file)
                                <tr>
                                    <th scope="row">
                                        <div class="media align-items-center">
                                            <a href="#" class="avatar mr-3 text-gradient">
                                                @if($file->isVideo())
                                                    <i class="fa fa-file-video"></i>
                                                @elseif($file->isImage())
                                                    <i class="fa fa-file-image"></i>
                                                @else
                                                    <i class="fa fa-file"></i>
                                                @endif
                                            </a>
                                            <div class="media-body">
                                                <a href="{{ route('file', ['slug' => $file->slug]) }}"><span class="mb-0 text-sm">{{ $file->name }}</span></a>
                                            </div>
                                        </div>
                                    </th>
                                    <td>
                                        {{ $file->formatedSize() }}
                                    </td>
                                    <td>
                                      <span class="badge badge-dot mr-4">
                                          @if($file->isVideo())
                                              <span class="badge badge-pill badge-success">Yes</span>
                                          @else
                                              <span class="badge badge-pill badge-danger">No</span>
                                          @endif
                                      </span>
                                    </td>
                                    <td class="text-right">
                                        <div class="dropdown">
                                            <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-ellipsis-v"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                @if($file->isVideo())
                                                <a class="dropdown-item" href="{{ route('file', ['slug' => $file->slug]) }}"><i class="fa fa-eye"></i> Show video</a>
                                                @endif
                                                <a class="dropdown-item" href="javascript:void(0)" data-toggle="modal" data-target="#modal-delete" onClick="deleteFile('{{ $file->slug }}', '{{ $file->token }}')"><i class="fa fa-trash"></i> Delete</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <div class="card-body">
                        <h5 class="card-title">
                            There is no file, you can upload files on homepage.
                        </h5>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modal-delete" aria-hidden="true">
        <div class="modal-dialog modal-danger modal-dialog-centered modal-" role="document">
            <div class="modal-content bg-gradient-danger">

                <div class="modal-header">
                    <h6 class="modal-title" id="modal-title-notification">Your attention is required</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>

                <div class="modal-body">

                    <div class="py-3 text-center">
                        <i class="fa fa-trash ni-3x"></i>
                        <h4 class="heading mt-4">This file will be deleted!</h4>
                        <p>Be careful you will not be able to go back.</p>
                    </div>

                </div>

                <div class="modal-footer">
                    <a href="#" class="btn btn-white" id="deleteLink">Delete it</a>
                    <button type="button" class="btn btn-link text-white ml-auto" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        function deleteFile(slug, token) {
            $('#deleteLink').attr('href', '{{ url('dashboard/files') }}/'+slug+'/'+token+'/delete')
        }
    </script>
@endsection
