@extends('dashboard.includes.default')

@section('title', trans('global.meta.title'))

@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection

@section('header')
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row">
                    <div class="col-xl-3 col-lg-6">
                        <div class="card card-stats mb-4 mb-xl-0">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">Downloads: Today</h5>
                                        <span class="h2 font-weight-bold mb-0">{{ Auth::user()->downloads()->whereDate('created_at', Carbon\Carbon::today())->count() }}</span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                                            <i class="fas fa-download"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6">
                        <div class="card card-stats mb-4 mb-xl-0">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">Available Money</h5>
                                        <span class="h2 font-weight-bold mb-0">${{ Auth::user()->availableMoney() }}</span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-warning text-white rounded-circle shadow">
                                            <i class="ni ni-money-coins"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6">
                        <div class="card card-stats mb-4 mb-xl-0">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">Space used</h5>
                                        <span class="h2 font-weight-bold mb-0">{{ \App\File::formatBytes(Auth::user()->spaceUsed()) }}</span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-yellow text-white rounded-circle shadow">
                                            <i class="fas fa-store"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6">
                        <div class="card card-stats mb-4 mb-xl-0">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">Most popular file</h5>
                                        <span class="h2 font-weight-bold mb-0">@if(\App\Download::mostDownloaded(Auth::user()->downloadsOnMyFiles())){{ \App\Download::mostDownloaded(Auth::user()->downloadsOnMyFiles())->filename }}@else{{ 'No files...' }}@endif</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-xl-8 mb-5 mb-xl-0">
            <div class="card bg-gradient-default shadow">
                <div class="card-header bg-transparent">
                    <div class="row align-items-center">
                        <div class="col">
                            <h6 class="text-uppercase text-light ls-1 mb-1">Overview</h6>
                            <h2 class="text-white mb-0">Downloads</h2>
                        </div>
                        <div class="col">
                            <ul class="nav nav-pills justify-content-end">
                                <li class="nav-item mr-2 mr-md-0" data-toggle="chart" data-target="#chart-downloads" data-prefix="" data-suffix="">
                                    <a href="#" class="nav-link py-2 px-3 active" data-toggle="tab" id="monthDownloads">
                                        <span class="d-none d-md-block">Month</span>
                                        <span class="d-md-none">M</span>
                                    </a>
                                </li>
                                <li class="nav-item" data-toggle="chart" data-target="#chart-downloads" data-prefix="" data-suffix="">
                                    <a href="#" class="nav-link py-2 px-3" data-toggle="tab" id="dayDownloads">
                                        <span class="d-none d-md-block">Day</span>
                                        <span class="d-md-none">D</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="chart">
                        <canvas id="chart-downloads" class="chart-canvas"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-4">
            <div class="card shadow">
                <div class="card-header bg-transparent">
                    <div class="row align-items-center">
                        <div class="col">
                            <h6 class="text-uppercase text-muted ls-1 mb-1">Money</h6>
                            <h2 class="mb-0">Incomes</h2>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <!-- Chart -->
                    <div class="chart">
                        <canvas id="chart-earned-money" class="chart-canvas"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-5">
            <div class="col-xl-8 mb-5 mb-xl-0">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Latest files</h3>
                            </div>
                            <div class="col text-right">
                                <a href="{{ route('dashboard.files') }}" class="btn btn-sm btn-primary">See all</a>
                            </div>
                        </div>
                    </div>
                    @if(Auth::user()->files()->orderBy('id', 'desc')->take(10)->count() > 0)
                    <div class="table-responsive">
                        <!-- Projects table -->
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">Filename</th>
                                <th scope="col">Visitors</th>
                                <th scope="col">Unique IP</th>
                                <th scope="col">Bounce rate</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach(Auth::user()->files()->orderBy('id', 'desc')->take(10)->get() as $file)
                                    <tr>
                                        <th scope="row">
                                            {{ $file->filename }}
                                        </th>
                                        <td>
                                            {{ $file->downloads()->count() }}
                                        </td>
                                        <td>
                                            {{ $file->downloads()->groupBy('ip')->count() }}
                                        </td>
                                        <td>
                                            <i class="fas fa-arrow-up text-success mr-3"></i> {{ $file->bounceRate() }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @else
                        <div class="card-body">
                            <div class="alert alert-darker">You do not have file</div>
                        </div>
                    @endif
                </div>
            </div>
            <div class="col-xl-4">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Social traffic</h3>
                            </div>
                            <div class="col text-right">
                                <a href="#!" class="btn btn-sm btn-primary">See all</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <h3>UNDER COUNSTRUCTION</h3>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('js')
    <script src="{{ url('vendor/chart.js/dist/Chart.min.js') }}"></script>
    <script src="{{ url('vendor/chart.js/dist/Chart.extension.js') }}"></script>
@endsection

@section('js2')
    <script type="text/javascript">
        $(document).ready(function(){
            var downloadChart = $('#chart-downloads').data('chart');
            var moneyChart = $('#chart-earned-money').data('chart');

            $('#monthDownloads').click(function(){
                updateDownloads('m');
            });

            $('#dayDownloads').click(function(){
                updateDownloads('d');
            });

            function updateDownloads(period){
                $.ajax({
                    url: "{{ route('download.stats', ['period' => '']) }}/"+period,
                    dataType: "json",
                }).done(function(datas) {
                    console.log(datas);
                    downloadChart.data = datas;
                    downloadChart.update();
                });
            }
            updateDownloads('m');

            function updateMoney(){
                $.ajax({
                    url: "{{ route('money.stats') }}",
                    dataType: "json",
                }).done(function(datas) {
                    console.log(datas);
                    moneyChart.data = datas;
                    moneyChart.update();
                });
            }

            updateMoney();
        })
    </script>
@endsection
