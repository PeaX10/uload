<!doctype html>
<html lang="{{ App::getLocale() }}">
<head>
    <meta charset="utf-8">
    <title>uLoad - @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="{{ @url('img/favicon.png') }}" rel="icon" type="image/png">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="{{ url('vendor/nucleo/css/nucleo.css') }}" rel="stylesheet">
    <link href="{{ url('vendor/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ URL::asset('css/argon.min.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ URL::asset('css/dashboard.css') }}" rel="stylesheet">
    @yield('css')
</head>

<body>
    @include('dashboard.includes.sidebar')
    <!-- Main content -->
    <div class="main-content">
        @include('dashboard.includes.menu')
        @yield('header')
        <div class="container-fluid mt--7">
            @yield('content')
            @include('dashboard.includes.footer')
        </div>
    </div>
    <script src="{{ URL::asset('vendor/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
    @yield('js')
    <script src="{{ URL::asset('js/argon.js') }}"></script>
    @yield('js2')
</body>
</html>
