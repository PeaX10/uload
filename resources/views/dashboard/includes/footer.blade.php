<footer class="footer">
    <div class="row align-items-center justify-content-xl-between">
        <div class="col-xl-6">
            <div class="copyright text-center text-xl-left text-muted">
                &copy; 2019 <a class="font-weight-bold ml-1" target="_blank">uLoad</a>
            </div>
        </div>
        <div class="col-xl-6">
            <ul class="nav nav-footer justify-content-center justify-content-xl-end">
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('about') }}">@lang('global.footer.about')</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('faq') }}">@lang('global.footer.faq')</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('contact') }}">@lang('global.footer.contact')</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('terms') }}">@lang('global.footer.terms')</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('privacy') }}">@lang('global.footer.privacy')</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('copyright') }}">@lang('global.footer.copyright')</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('report') }}">@lang('global.footer.report')</a>
                </li>
            </ul>
        </div>
    </div>
</footer>
