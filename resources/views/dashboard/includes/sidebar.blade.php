<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
    <div class="container-fluid">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand pt-0" href="{{ url('/') }}">
            <img src="{{ url('img/logo.png') }}" class="navbar-brand-img" alt="...">
        </a>

        <ul class="nav align-items-center d-md-none">
            <li class="nav-item dropdown">
                <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="media align-items-center">
                  <span class="avatar avatar-sm rounded-circle bg-gradient-indigo">
                      <i class="fas fa-user"></i>
                    </span>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                    <div class=" dropdown-header noti-title">
                        <h6 class="text-overflow m-0">Welcome!</h6>
                    </div>
                    <a href="" class="dropdown-item">
                        <i class="ni ni-single-02"></i>
                        <span>My profile</span>
                    </a>
                    <a href="" class="dropdown-item">
                        <i class="ni ni-settings-gear-65"></i>
                        <span>Settings</span>
                    </a>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="{{ route('logout') }}" class="dropdown-item">
                        <i class="ni ni-user-run"></i>
                        <span>Logout</span>
                    </a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{ strtoupper(App::getLocale()) }}
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right" aria-labelledby="navbarDropdown">
                    @foreach(explode(',', env('LANG_ALLOWED')) as $lang)
                        <a class="dropdown-item" @if($lang != App::getLocale()) href="{{ url('lang/'.$lang) }}" @endif>{{ strtoupper($lang) }}</a>
                    @endforeach
                </div>
            </li>
        </ul>
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
            <div class="navbar-collapse-header d-md-none">
                <div class="row">
                    <div class="col-6 collapse-brand">
                        <a href="{{ route('home') }}">
                            <img src="{{ url('img/logo.png') }}">
                        </a>
                    </div>
                    <div class="col-6 collapse-close">
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>

            <form class="mt-4 mb-3 d-md-none">
                <div class="input-group input-group-rounded input-group-merge">
                    <input type="search" class="form-control form-control-rounded form-control-prepended" placeholder="Search" aria-label="Search">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <span class="fa fa-search"></span>
                        </div>
                    </div>
                </div>
            </form>

            <ul class="navbar-nav">
                <li class="nav-item @if( empty($activePage) or $activePage == "dashboard"){{ 'active' }}@endif">
                    <a class="nav-link " href="{{ route('dashboard.index') }}">
                        <i class="fas fa-tachometer-alt text-light"></i> Dashboard
                    </a>
                </li>
                <li class="nav-item @if( !empty($activePage) and $activePage == "files"){{ 'active' }}@endif">
                    <a class="nav-link" href="{{ route('dashboard.files') }}">
                        <i class="ni ni-folder-17 text-light"></i> Files
                    </a>
                </li>
                <li class="nav-item @if( !empty($activePage) and $activePage == "finance"){{ 'active' }}@endif">
                    <a class="nav-link" href="{{ route('dashboard.finance') }}">
                        <i class="ni ni-money-coins text-light"></i> Finance
                    </a>
                </li>
                <li class="nav-item @if( !empty($activePage) and $activePage == "settings"){{ 'active' }}@endif">
                    <a class="nav-link" href="#">
                        <i class="ni ni-settings-gear-65 text-light"></i> Settings
                    </a>
                </li>
            </ul>
    </div>
</nav>
