@extends('emails.layout')

@section('content')
    <h2>@lang('email.sign_up.title')</h2>
    <hr>
    <p>@lang('email.sign_up.text')</p>
    <a>@lang('email.sign_up.email') <b>{{ $data['email'] }}</b></a><br>
    <a>@lang('email.sign_up.password') <b>{{ $data['password'] }}</b></a><br>
    <p>@lang('email.sign_up.thx')</p>
@endsection