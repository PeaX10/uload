@extends('emails.layout')

@section('content')
    <h2>@lang('email.reset_password.title')</h2>
    <hr>
    <p>@lang('email.reset_password.text')</p>
    <a href="{{ url('/resetpassword/'.$data['email'].'/'.$data['token']) }}">{{ url('/resetpassword/'.$data['email'].'/'.$data['token']) }}</a>
    <p>@lang('email.reset_password.thx')</p>
@endsection