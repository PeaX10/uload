@extends('emails.layout')

@section('content')
    <h3>Demande de contact</h3>
    <hr>
    <a>Nom : <b>{{ $data['name'] }}</b></a><br>
    <a>E-mail : <b>{{ $data['email'] }}</b></a><br>
    <a>Adresse IP : <b>{{ $data['ip'] }}</b></a>
    <hr>
    <h3><i>Message :</i></h3>
    <p>{!! nl2br($data['message'])  !!}</p>
@endsection