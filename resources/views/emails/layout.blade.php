<!DOCTYPE html>
<html lang="{{ App::getLocale() }}">
<head>
    <meta charset="utf-8">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<style>
    @import url('https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i');
    *{
        font-family: 'Source Sans Pro', sans-serif;
        color: #7A7A7A;
        font-weight:300 !important;
    }
    body{
        background: #32a5fd;
        background: -moz-linear-gradient(-45deg, #32a5fd 0%, #7271fb 50%, #9561fb 100%);
        background: -webkit-linear-gradient(-45deg, #32a5fd 0%,#7271fb 50%,#9561fb 100%);
        background: linear-gradient(135deg, #32a5fd 0%,#7271fb 50%,#9561fb 100%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#32a5fd', endColorstr='#9561fb',GradientType=1 );
    }

    b{
        color:#1b1e21;
        font-weight: bold;
    }

    .content{
        width: 90%;
        margin:auto;
        margin-top:30px;
        margin-bottom: 30px;
        padding: 20px;
        background: #FFF;
        border-radius: 5px;
    }

    .logo img{
        max-width: 120px;
    }
</style>
<div class="content">
    <div class="logo">
        <img src="{{ url('img/logo.png') }}">
    </div>
    @yield('content')
</div>
</body>