@extends('pages.includes.default')

@section('title', trans('global.forgot_password.title'))

@section('content')
    <div class="container">
        <div class="page">
            <h1>@lang('global.forgot_password.title')</h1>
            <hr>
            {!! Form::open() !!}
            @if(Session::has('no_email_error'))
                <div class="alert alert-danger" role="alert">
                    @lang('global.forgot_password.no_email_error')
                </div>
            @elseif(Session::has('email_sended_success'))
                <div class="alert alert-success" role="alert">
                    @lang('global.forgot_password.email_sended_success')
                </div>
            @endif
            {!! Form::text('email')->placeholder(trans('global.register.email'))->type('email')->attrs(['required' => 'required']) !!}
            {!! NoCaptcha::renderJs(App::getLocale()) !!}
            {!! NoCaptcha::display() !!}
            @if ($errors->has('g-recaptcha-response'))
                <span class="help-block">
                    <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                </span>
            @endif
            <hr>
            {!! Form::submit(trans('global.forgot_password.send'), 'danger')->lg()->outline() !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection