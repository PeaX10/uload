@extends('pages.includes.default')

@section('title', trans('global.register.title'))

@section('content')
    <div class="container">
        <div class="page">
            <h1>@lang('global.register.title')</h1>
            <hr>
            {!! Form::open() !!}
            {!! Form::text('email')->placeholder(trans('global.register.email'))->type('email')->attrs(['required' => 'required']) !!}
            {!! Form::text('password')->placeholder(trans('global.register.password'))->type('password')->attrs(['required' => 'required']) !!}
            {!! Form::text('password_2')->placeholder(trans('global.register.password_2'))->type('password')->attrs(['required' => 'required']) !!}
            <div class="text-left">{!! Form::checkbox('accept_terms', trans('global.register.accept_terms'))!!}</div>
            {!! NoCaptcha::renderJs(App::getLocale()) !!}
            {!! NoCaptcha::display() !!}
            @if ($errors->has('g-recaptcha-response'))
                <span class="help-block">
                    <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                </span>
            @endif
            <hr>
            {!! Form::submit(trans('global.register.send'), 'danger')->lg()->outline() !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection