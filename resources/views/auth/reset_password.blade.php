@extends('pages.includes.default')

@section('title', trans('global.reset_password.title'))

@section('content')
    <div class="container">
        <div class="page">
            <h1>@lang('global.reset_password.title')</h1>
            <hr>
            {!! Form::open() !!}
            {!! Form::text('r_password')->placeholder(trans('global.reset_password.password'))->type('password')->attrs(['required' => 'required']) !!}
            {!! Form::text('password_2')->placeholder(trans('global.reset_password.password_2'))->type('password')->attrs(['required' => 'required']) !!}
            <hr>
            {!! Form::submit(trans('global.reset_password.send'))->lg()->outline() !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection