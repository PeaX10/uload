<?php

return [

    'reset_password' => [
        'title' => 'Reset my password',
        'text' => 'Hi,<br>We\'ve received a request to reset your password. If you didn\'t make the request, just ignore this email. Otherwise, you can reset your password using this link:<br>',
        'thx' => 'Thanks,<br>The uLoad Team',
    ],
    'sign_up' => [
        'title' => 'Welcolme to uLoad',
        'text' => 'Hello and welcome to uLoad.<br>You can now start monetizing your content by accessing your <a href="'.url('/dashboard').'">dashboard</a>.<br><br><strong>Reminder:</strong>',
        'email' => 'Email:',
        'password' => 'Password:',
        'thx' => 'Thanks,<br>The uLoad Team',
    ],

];
