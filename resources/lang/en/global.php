<?php

return [

    'meta'   => [
        'title' =>'Share what you love, free.',
    ],

    'header' => [
        'help' => 'Help',
        'sign_up' => 'Sign Up (& Earn Money)',
        'login' => 'Login',
        'dashboard' => 'Dashboard',
        'logout' => 'Logout'
    ],

    'login' => [
        'title' => 'Login to your account',
        'remember_me' => 'Remember me',
        'send' => 'SIGN IN',
        'forgotpass' => 'forget password ?',
        'login_failed' => 'invalid credentials !'
    ],

    'footer' => [
        'about' => 'About',
        'faq' => 'FAQ',
        'contact' => 'Contact',
        'terms' => 'Terms',
        'privacy' => 'Privacy',
        'copyright' => 'Copyright',
        'report' => 'Report'
    ],

    // STATIC PAGES
    'home' => [
        'h1' => 'Share what you <b>love</b>, free',
        'drag_drop' => 'Browse or <b>drag & drop files</b>',
        'file_size' => '5 GB max',
        'alert_danger' => 'An error has occurred !',
        'alert_success' => 'The operation was successful !',
        'your_files' => 'Your files',
        'file_too_big' => 'Filesize is too big',
        'error_file_deleted' => 'An error occurred while deleting the file !',
        'delete_modal_title' => 'Delete file',
        'delete_modal_text' => 'Are you sure you want to delete this file ?',
        'delete_modal_close' => 'Cancel',
        'delete_modal_btn' => 'Delete !',
        'file_sizes' => '[\'kB\',\'MB\',\'GB\',\'TB\',\'PB\',\'EB\',\'ZB\',\'YB\']
                : [\'KiB\',\'MiB\',\'GiB\',\'TiB\',\'PiB\',\'EiB\',\'ZiB\',\'YiB\']',
        'ready_in' => 'Ready in'
    ],
    'about' => [
        'title_1' => 'THE MISSION',
        'text_1' => '<h5>To bring people closer using the power of sharing.</h5>',
        'title_2' => 'THE STORY',
        'text_2' => '<h5>Launched in 2018, uLoad is the first file hosting manager that truly gives an opportunity for anyone around the world to share privately what they love.</h5>
            <br>
            <h5>We buck the traditional file hosting system in favor of a smart - and free - way to host files. It\'s not only free. It\'s rewarding in cash thanks to our ad-technology displayed on your files.</h5>
            <br>
            <h5>Our values are clear. We will always:</h5>
            <ul>
                <li>Be free</li>
                <li>Be a safe and private place for everyone</li>
                <li>Provide the best hosting/streaming service as we can</li>
            </ul>',
    ],
    'faq' => [
        'title_1' => 'WHAT IS ULOAD?',
        'text_1' => '<h5>uLoad is a free, unlimited and rewarding file storage and video stream cloud service. Store any file from your devices on the cloud and share them with your friends and family.</h5>',
        'title_2' => 'WHAT TYPES OF FILES ARE SUPPORTED?',
        'text_2' => '<h5>You can upload any file you have on your computer or media device.</h5>',
        'title_3' => 'WHAT IS STREAMABLE?',
        'text_3' => '<h5>Any video file that can be converted into a high quality streamable video (avi, mp4, mkv, 3gp, mov, mpeg, mpg, xvid, flv, divx).</h5>',
        'title_4' => 'UP TO WHICH FILESIZE CAN I UPLOAD MY FILES?',
        'text_4' => '<h5>While others do 1 GB, we do 5 GB maximum.</h5>',
        'title_5' => 'ANY DOWNLOAD OR STREAM SPEED LIMITATIONS?',
        'text_5' => '<h5>Nop! You get it as fast as your connection can.</h5>',
        'title_6' => 'HOW LONG DO YOU HOST MY FILES?',
        'text_6' => '<h5>Files are hosted forever unless they are inactive. Inactive streamable files are removed after 60 days, non-streamable files after 30 days.</h5>',
        'title_7' => 'WHAT KIND OF FILES AREN\'T ALLOWED?',
        'text_7' => '<h5>We do not allow the upload of adult and copyrighted content.</h5>',
    ],
    'contact' => [
        'description' => 'For general or business inquiries, please fill out the following form to contact us. We will get back to you as soon as possible. To report a copyright abuse, please use our special form at <a href="'.url('report').'">uload.io/report</a>.',
        'name' => 'Name',
        'email' => 'Email',
        'message' => 'Please enter your message',
        'send' => 'Send it!',
        'mail_sended' => 'Your email was successfully sent!'
    ],
    'terms' => [
        'title_1' => 'TERMS',
        'text_1' => '<h5>This website is operated by uLoad also known as uLoad.io. Throughout the site, the terms “we”, “us” and “our” refers to streamango. streamango offers this website, tools and services available from this site to you, the user, conditioned upon your acceptance of all terms, conditions, policies and notices stated here. The term “uLoad” throughout the site refers to uLoad.io</h5>
                    <h5>By visiting our site you engage in our “Service” and agree to be bound by the following terms and conditions (“Terms of Service”, “Terms”), including those additional terms and conditions and policies referenced herein and/or available by hyperlink. These Terms of Service apply to all users of the site, including without limitation users who are visitors, viewers and/or contributors of content.</h5>
                    <h5>Please read these Terms of Service carefully before accessing or using our website. By accessing or using any part of the site, you agree to be bound by these Terms of Service. If you do not agree to all the terms and conditions of this agreement, then you may not access the website or use any services.</h5>
                    <h5>Any new features or tools which are added to the current site shall also be subject to the Terms of Service. You can review the most current version of the Terms of Service at any time on this page. We reserve the right to update, change or replace any part of these Terms of Service by posting updates and/or changes to our website. It is your responsibility to check this page periodically for changes. Your continued use of or access to the website following the posting of any changes constitutes acceptance of those changes.</h5>
                    <h5>uLoad reserves the right to remove any files that compromise the security of the server, use excess bandwidth, or are otherwise malignant. The following types of files may not be uploaded under any circumstances:</h5>
                    <h5>Files that infringe on the copyrights of any entity. Files that are illegal and/or are in violation of any laws.</h5>',
        'title_2' => 'TERMS OF USAGE',
        'text_2' => '<h5>uLoad assumes no liability for lost or corrupt links, files or misplaced file URLs. It is user\'s responsibility to keep track of this information.</h5>',
        'title_3' => 'LEGAL POLICY',
        'text_3' => '<h5>These Terms of Service are subject to change without prior warning. By using uLoad, user agrees not to involve uLoad in any type of legal action. uLoad directs full legal responsibility of the contents of the files that are uploaded to uLoad to individual users, and will cooperate with copyright owners and law enforcement entities in the case that uploaded files are deemed to be in violation of these Terms of Service.</h5>
                        <h5>All files are copyrighted to their respective owners. uLoad is not responsible for the content of any uploaded files, nor is it in affiliation with any entities that may be represented in the uploaded files.</h5>',
        'title_4' => '3RD PARTY LINKS',
        'text_4' => '<h5>Certain content made available via our Service may include materials from third-parties.</h5>
                    <h5>Third-party links on this site may direct you to third-party websites that are not affiliated with us. We are not responsible for examining or evaluating the content or accuracy and we do not warrant and will not have any liability or responsibility for any third-party materials or websites, or for any other materials, products, or services of third-parties.</h5>
                    <h5>We are not liable for any harm or damages related to the purchase or use of goods, services, resources, content, or any other transactions made in connection with any third-party websites. Please review carefully the third-party\'s policies and practices and make sure you understand them before you engage in any transaction. Complaints, claims, concerns, or questions regarding third-party products should be directed to the third-party.</h5>',
        'title_5' => 'PRIVACY POLICY',
        'text_5' => '<h5>uLoad respects the visitors privacy and is committed to protecting it. Customers can visit most pages and content links on the uLoad website, without identifying themselves or providing any personal or financial information. However, if a user wishes to open an account, we requires the user to provide uLoad with certain non- public personal e.g. E-Mail Address. uLoad will never disclose Personal Information of registered users to non-affiliated third party entities.</h5>',
    ],
    'register' => [
        'title' => 'Sign up',
        'email' => 'Email',
        'password' => 'Password',
        'password_2' => 'Enter password again',
        'accept_terms' => 'I understand and agree to the <a href="'.url('terms').'">terms and conditions</a> of uLoad.io.',
        'send' => 'Sign up!',
    ],
    'forgot_password' => [
        'title' => 'Forgot password',
        'send' => 'Send',
        'no_email_error' => 'No account was created with this email address',
        'email_sended_success' => 'An email with a reset link has been sent',
    ],
    'reset_password' => [
        'title' => 'Reset password',
        'send' => 'Reset',
        'password' => 'New password',
        'password_2' => 'New password again',
    ],
];
