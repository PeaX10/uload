<?php

return [

    'reset_password' => [
        'title' => 'Réinitialiser mon mot de passe',
        'text' => 'Hey,<br>Nous avons reçu une demande de réinitialisation de ton mot de passe. Si tu n\'as pas fait cette demande, tu peux simplement ignorer cet e-mail. Sinon, tu peux suivre ce lien pour réinitialiser ton mot de passe:<br>',
        'thx' => 'Merci,<br>L\'Equipe uLoad',
    ],
    'sign_up' => [
        'title' => 'Bienvenue sur uLoad',
        'text' => 'Bonjour et bienvenue sur uLoad.<br>Vous pouvez dès à présent héberger des fichiers et les monétiser en accèdant à votre <a href="'.url('/dashboard').'">tableau de bord</a>.<br><br></strong>Rappel:</strong>',
        'email' => 'E-mail:',
        'password' => 'Mot de passe:',
        'thx' => 'Merci,<br>L\'Equipe uLoad',
    ],

];
