<?php

return [

    'meta'   => [
        'title' =>'Partagez ce que vous aimez, gratuitement.',
    ],

    'header' => [
        'help' => 'Aide',
        'sign_up' => 'Inscription (& Gagnez des €)',
        'login' => 'Connexion',
        'dashboard' => 'Tableau de board',
        'logout' => 'Déconnexion'
    ],

    'login' => [
        'title' => 'Connexion à votre compte',
        'remember_me' => 'Se souvenir de moi',
        'send' => 'CONNEXION',
        'forgotpass' => 'mot de passe perdu ?',
        'login_failed' => 'La combinaison est incorrecte !'
    ],

    'footer' => [
        'about' => 'À propos',
        'faq' => 'FAQ',
        'contact' => 'Contact',
        'terms' => 'CGU',
        'privacy' => 'Confidentialité',
        'copyright' => 'Copyright',
        'report' => 'Signalement'
    ],

    // STATIC PAGES
    'home' => [
        'h1' => 'Partagez ce que vous <b>aimez</b>, gratuitement',
        'drag_drop' => '<b>Parcourir</b> ou <b>glisser & déposer des fichiers</b>',
        'file_size' => '5 GO max',
        'alert_danger' => 'Une erreur est survenue !',
        'alert_success' => 'L\'opération a été un succès !',
        'your_files' => 'Vos fichiers',
        'file_too_big' => 'Le fichier est trop gros',
        'error_file_deleted' => 'Une erreur est survenue lors de la suppresssion du fichier!',
        'delete_modal_title' => 'Supprimer un fichier',
        'delete_modal_text' => 'Êtes-vous sûr de vouloir supprimer ce fichier ?',
        'delete_modal_close' => 'Annuler',
        'delete_modal_btn' => 'Supprimer !',
        'file_sizes' => '[\'KO\',\'MO\',\'GO\',\'TO\',\'PO\',\'EO\',\'ZO\',\'YO\']
                : [\'KiO\',\'MiO\',\'GiO\',\'TiO\',\'PiO\',\'EiO\',\'ZiO\',\'YiO\']',
        'ready_in' => 'Prêt dans'
    ],
    'about' => [
        'title_1' => 'NOTRE MISSION',
        'text_1' => '<h5>Rapprocher le monde grâce au pouvoir du partage.</h5>',
        'title_2' => 'NOTRE HISTOIRE',
        'text_2' => '<h5>Lancé en 2018, uLoad est le premier hébergeur de fichier qui offre une véritable opportunité à n\'importe qui dans le monde de partager ce qu\'il ou elle aime.</h5>
            <br>
            <h5>Nous avons remplacé le système d\'hébergement traditionnel payant et bridé par un moyen intelligent, et gratuit. Et ce n\'est pas seulement gratuit. C\'est récompensant en argent grâce à nos technologies publicitaires propriétaires.</h5>
            <br>
            <h5>Nos valeurs sont claires. Nous serons toujours :</h5>
            <ul>
                <li>Gratuit</li>
                <li>Un endroit sûr et privé pour tout le monde</li>
                <li>À la pointe de l\'hébergement et du streaming</li>
            </ul>',
    ],
    'faq' => [
        'title_1' => 'QU\'EST-CE QUE ULOAD ?',
        'text_1' => '<h5>uLoad is un hébergeur de fichier et streameur de vidéo sur cloud gratuit, illimité et récompensant. Stocker vos fichiers depuis vos appareils sur le cloud et partagez les anonymement avec vos proches.</h5>',
        'title_2' => 'QUELS TYPES DE FICHIERS SONT SUPPORTÉS ?',
        'text_2' => '<h5>Vous pouvez uploader n\'importe quel fichier présent sur votre ordinateur ou mobile.</h5>',
        'title_3' => 'QU\'EST-CE QUI EST STREAMABLE ?',
        'text_3' => '<h5>Tout fichier vidéo pouvant être converti en vidéo haute-qualité (avi, mp4, mkv, 3gp, mov, mpeg, mpg, xvid, flv, divx).</h5>',
        'title_4' => 'DE QUELLE TAILLE PEUVENT ÊTRE MES FICHIERS ?',
        'text_4' => '<h5>Pendant que les autres acceptent au maximum 1 Go, chez nous c\'est 5 Go par fichier. Profitez-en ;)</h5>',
        'title_5' => 'Y A-T-IL DES LIMITATION DE VITESSE ?',
        'text_5' => '<h5>Non. Vous téléchargez ou streamez au maximum de votre connexion Internet.</h5>',
        'title_6' => 'COMBIEN DE TEMPS SONT STOCKÉS MES FICHIERS ?',
        'text_6' => '<h5>Vos fichiers sont stockés pour toujours tant qu\'ils sont actifs (c\'est à dire téléchargés). Un fichier streamable non téléchargé ou streamé depuis 60 jours est supprimé. Et un fichier standard est supprimé après 30 jours d\'inactivité.</h5>',
        'title_7' => 'QUELS TYPES DE FICHIERS SONT INTERDITS ?',
        'text_7' => '<h5>Nous n\'autorisons pas les contenus pornographiques et piratés.</h5>',
    ],
    'contact' => [
        'description' => 'Pour toute demande ou proposition commerciale, merci de nous contacter via le formulaire ci-dessous. Pour signaler un abus, merci d\'utiliser le formulaire dédié sur <a href="'.url('report').'">uload.io/report</a>.',
        'name' => 'Nom',
        'email' => 'E-mail',
        'message' => 'Merci d\'entrer votre message',
        'send' => 'Envoyer!',
        'mail_sended' => 'Votre demande de contact a bien été prise en compte !'
    ],
    'terms' => [
        'title_1' => 'TERMS',
        'text_1' => '<h5>This website is operated by uLoad also known as uLoad.io. Throughout the site, the terms “we”, “us” and “our” refers to streamango. streamango offers this website, tools and services available from this site to you, the user, conditioned upon your acceptance of all terms, conditions, policies and notices stated here. The term “uLoad” throughout the site refers to uLoad.io</h5>
                    <h5>By visiting our site you engage in our “Service” and agree to be bound by the following terms and conditions (“Terms of Service”, “Terms”), including those additional terms and conditions and policies referenced herein and/or available by hyperlink. These Terms of Service apply to all users of the site, including without limitation users who are visitors, viewers and/or contributors of content.</h5>
                    <h5>Please read these Terms of Service carefully before accessing or using our website. By accessing or using any part of the site, you agree to be bound by these Terms of Service. If you do not agree to all the terms and conditions of this agreement, then you may not access the website or use any services.</h5>
                    <h5>Any new features or tools which are added to the current site shall also be subject to the Terms of Service. You can review the most current version of the Terms of Service at any time on this page. We reserve the right to update, change or replace any part of these Terms of Service by posting updates and/or changes to our website. It is your responsibility to check this page periodically for changes. Your continued use of or access to the website following the posting of any changes constitutes acceptance of those changes.</h5>
                    <h5>uLoad reserves the right to remove any files that compromise the security of the server, use excess bandwidth, or are otherwise malignant. The following types of files may not be uploaded under any circumstances:</h5>
                    <h5>Files that infringe on the copyrights of any entity. Files that are illegal and/or are in violation of any laws.</h5>',
        'title_2' => 'TERMS OF USAGE',
        'text_2' => '<h5>uLoad assumes no liability for lost or corrupt links, files or misplaced file URLs. It is user\'s responsibility to keep track of this information.</h5>',
        'title_3' => 'LEGAL POLICY',
        'text_3' => '<h5>These Terms of Service are subject to change without prior warning. By using uLoad, user agrees not to involve uLoad in any type of legal action. uLoad directs full legal responsibility of the contents of the files that are uploaded to uLoad to individual users, and will cooperate with copyright owners and law enforcement entities in the case that uploaded files are deemed to be in violation of these Terms of Service.</h5>
                        <h5>All files are copyrighted to their respective owners. uLoad is not responsible for the content of any uploaded files, nor is it in affiliation with any entities that may be represented in the uploaded files.</h5>',
        'title_4' => '3RD PARTY LINKS',
        'text_4' => '<h5>Certain content made available via our Service may include materials from third-parties.</h5>
                    <h5>Third-party links on this site may direct you to third-party websites that are not affiliated with us. We are not responsible for examining or evaluating the content or accuracy and we do not warrant and will not have any liability or responsibility for any third-party materials or websites, or for any other materials, products, or services of third-parties.</h5>
                    <h5>We are not liable for any harm or damages related to the purchase or use of goods, services, resources, content, or any other transactions made in connection with any third-party websites. Please review carefully the third-party\'s policies and practices and make sure you understand them before you engage in any transaction. Complaints, claims, concerns, or questions regarding third-party products should be directed to the third-party.</h5>',
        'title_5' => 'PRIVACY POLICY',
        'text_5' => '<h5>uLoad respects the visitors privacy and is committed to protecting it. Customers can visit most pages and content links on the uLoad website, without identifying themselves or providing any personal or financial information. However, if a user wishes to open an account, we requires the user to provide uLoad with certain non- public personal e.g. E-Mail Address. uLoad will never disclose Personal Information of registered users to non-affiliated third party entities.</h5>',
    ],
    'register' => [
        'title' => 'Inscription',
        'email' => 'E-mail',
        'password' => 'Mot de passe',
        'password_2' => 'Confirmation du mot de passe',
        'accept_terms' => 'Je reconnais avoir reçu, pris connaissance et accepté sans réserve les <a href="'.url('terms').'">CGU</a> du service uLoad.io',
        'send' => 'Inscription !',
    ],
    'forgot_password' => [
        'title' => 'Mot de passe oublié',
        'send' => 'Envoyer',
        'no_email_error' => 'Aucun compte n\'existe avec cette adresse mail',
        'email_sended_success' => 'Un e-mail contenant un lien de réinitialisation vient de vous être envoyé',
    ]
];
