<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveEarnedAddDataToPayment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('benefits', function (Blueprint $table) {
            $table->dropColumn('earned');
        });

        Schema::table('payments', function (Blueprint $table) {
            $table->text('data')->after('target');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('benefits', function (Blueprint $table) {
            $table->boolean('earned')->default(false);
        });

        Schema::table('payments', function (Blueprint $table) {
            $table->dropColumn('data');
        });
    }
}
