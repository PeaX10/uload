<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/init', 'PageController@init')->name('init'); // TODO : Delete après
Route::get('/', 'PageController@index')->name('home');
Route::get('/lang/{id}', 'PageController@lang')->name('lang.choose');
Route::get('/about', 'PageController@about')->name('about');
Route::get('/faq', 'PageController@faq')->name('faq');
Route::get('/contact', 'PageController@contact')->name('contact');
Route::post('/contact', 'PageController@postContact')->name('contact.post');
Route::get('/terms', 'PageController@terms')->name('terms');
Route::get('/privacy', 'PageController@privacy')->name('privacy');
Route::get('/copyright', 'PageController@copyright')->name('copyright');
Route::get('/report', 'PageController@report')->name('report');
Route::get('/register', 'AuthController@register')->name('register');
Route::post('/register', 'AuthController@postRegister')->name('register.post');
Route::get('/logout', 'AuthController@logout')->name('logout');
Route::post('/login', 'AuthController@postLogin')->name('login');
Route::post('/login', 'AuthController@postLogin')->name('login');
Route::get('/forgotpassword', 'AuthController@forgotPassword')->name('forgot_password');
Route::post('/forgotpassword', 'AuthController@postForgotPassword')->name('forgot_password.post');
Route::get('/resetpassword/{email}/{token}', 'AuthController@resetPassword')->name('reset_password');
Route::post('/resetpassword/{email}/{token}', 'AuthController@postResetPassword')->name('reset_password.post');
Route::post('/upload', 'UploadController@ajax')->name('upload.ajax');
Route::get('/d/{slug}/{token}', 'UploadController@delete')->name('upload.delete');
Route::get('/f/{slug}', 'PageController@file')->name('file');
Route::get('/d/{slug}', 'DownloadController@download')->name('download');
Route::get('/json/downloads/stats/{period}', 'DownloadController@stats')->name('download.stats');
Route::get('/json/money/stats', 'Dashboard\FinanceController@stats')->name('money.stats');
Route::get('/v/{slug}', 'PageController@video')->name('video');

Route::group(['prefix' => 'dashboard', 'as' => 'dashboard.', 'middleware' => 'noguest'], function(){
    Route::get('/', 'Dashboard\HomeController@index')->name('index');
    Route::get('/files', 'Dashboard\FileController@index')->name('files');
    Route::get('/files/{slug}/{token}/delete', 'Dashboard\FileController@delete')->name('files.delete');
    Route::get('/finance', 'Dashboard\FinanceController@index')->name('finance');
    Route::get('/finance/recover', 'Dashboard\FinanceController@recover')->name('finance.recover');
    Route::post('/finance/recover/paypal', 'Dashboard\FinanceController@paypal')->name('finance.recover.paypal');
});
